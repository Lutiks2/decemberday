<?php
/**
 * december Theme Customizer
 *
 * @package december
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function december_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'december_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'december_customize_partial_blogdescription',
		) );
	}

    //// welcome settings
    $wp_customize->add_section('welcome', array(
        'title' => __('Welcome settings'),
        'description' => __('Add settings here'),
        'panel' => '', // Not typically needed.
        'priority' => 20,
        'capability' => 'edit_theme_options',
        'theme_supports' => '', // Rarely needed.
    ));
// add  welcome background
    $wp_customize->add_setting('set_background', array(
        'type' => 'theme_mod', // or 'option'
        'capability' => 'edit_theme_options',
        'theme_supports' => '', // Rarely needed.
        'default' => '',
        'transport' => 'refresh', // or postMessage
        'sanitize_callback' => '',
        'sanitize_js_callback' => '', // Basically to_json.
    ));
    $wp_customize->add_control(new WP_Customize_Media_Control($wp_customize, 'set_background', array(
        'label' => __('Featured Home Page Image', 'december'),
        'section' => 'welcome',
        'type' => 'background',
    )));
}
add_action( 'customize_register', 'december_customize_register' );


/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function december_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function december_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function december_customize_preview_js() {
	wp_enqueue_script( 'december-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'december_customize_preview_js' );
